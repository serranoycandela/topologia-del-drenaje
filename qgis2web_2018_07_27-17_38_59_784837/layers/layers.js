var wms_layers = [];
var format_DrenProf_sacmex_cg_0 = new ol.format.GeoJSON();
var features_DrenProf_sacmex_cg_0 = format_DrenProf_sacmex_cg_0.readFeatures(json_DrenProf_sacmex_cg_0, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_DrenProf_sacmex_cg_0 = new ol.source.Vector({
    attributions: [new ol.Attribution({html: '<a href=""></a>'})],
});
jsonSource_DrenProf_sacmex_cg_0.addFeatures(features_DrenProf_sacmex_cg_0);var lyr_DrenProf_sacmex_cg_0 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_DrenProf_sacmex_cg_0, 
                style: style_DrenProf_sacmex_cg_0,
                title: '<img src="styles/legend/DrenProf_sacmex_cg_0.png" /> DrenProf_sacmex_cg'
            });

lyr_DrenProf_sacmex_cg_0.setVisible(true);
var layersList = [lyr_DrenProf_sacmex_cg_0];
lyr_DrenProf_sacmex_cg_0.set('fieldAliases', {'FID': 'FID', });
lyr_DrenProf_sacmex_cg_0.set('fieldImages', {'FID': 'TextEdit', });
lyr_DrenProf_sacmex_cg_0.set('fieldLabels', {'FID': 'no label', });
lyr_DrenProf_sacmex_cg_0.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});